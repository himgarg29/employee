package de.fr.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fr.model.Employee;
import de.fr.repository.jpa.EmployeeRepository;
import de.fr.service.EmployeeService;

@Api(value="Employee Management System", description="Operations pertaining to employee in Employee Management System")
@RestController
//@ComponentScan(basePackages = "de.fr.service")
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@ApiOperation(value = "View a list of available employees", response = List.class)
	@GetMapping(value="/list")
    public List<Employee> getAll() {
		
        return employeeService.getAllEmployees();
    }
	
	@RequestMapping(value="/{id}",  method=RequestMethod.GET)
    public Optional<Employee> getEmployeeById(@PathVariable Long id) {
		
        return employeeService.getEmployeeById(id);
    }
	
	/*@ApiOperation(value = "Search employee by first name")
	@GetMapping(params = "firstName")
    public List<Employee> getEmployeeByFirstName(
    		@ApiParam(value = "first name of employee", required = true) 
    		@RequestParam(value = "firstName") String firstName) {
		
        return employeeService.getEmployeeByFirstName(firstName);
    }*/
	
	@ApiOperation(value = "Search employee by either first or last name")
	@GetMapping
    public List<Employee> getEmployeeByFirstOrLastName(
    		@ApiParam(value = "either first or last name of employee", required = true) 
    		@RequestParam(value = "name") String name) {
		
        return employeeService.getEmployeeByFirstOrLastName(name);
    }
}
