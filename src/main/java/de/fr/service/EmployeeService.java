package de.fr.service;

import java.util.List;
import java.util.Optional;

import de.fr.model.Employee;

public interface EmployeeService {

	public List<Employee> getAllEmployees();
	public Optional<Employee> getEmployeeById(long Id);
	public List<Employee> getEmployeeByFirstName(String firstName);
	public List<Employee> getEmployeeByFirstOrLastName(String name);
}
