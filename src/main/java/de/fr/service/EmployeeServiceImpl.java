package de.fr.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fr.model.Employee;
import de.fr.repository.jpa.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	public List<Employee> getAllEmployees()
	{
		return employeeRepository.findAll();
	}
	public Optional<Employee> getEmployeeById(long id)
	{
		return employeeRepository.findById(id);
	}
	public List<Employee> getEmployeeByFirstName(String firstName)
	{
		return employeeRepository.findByFirstName(firstName);
	}
	public List<Employee> getEmployeeByFirstOrLastName(String name)
	{
		return employeeRepository.findByFirstNameOrLastName(name, name);
	}
}
