package de.fr;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import de.fr.model.Employee;
import de.fr.repository.jpa.EmployeeRepository;

@SpringBootApplication
//@EnableMongoRepositories
//@ComponentScan(basePackages = "de.fr")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

	}
	
	@Bean
    public CommandLineRunner commandLineRunner(EmployeeRepository repository) {
        return args -> {
        	
        	/*repository.save(new Employee("himanshu", "garg"));
        	repository.save(new Employee("nikhil", "garg"));
        	repository.save(new Employee("rachid", "el"));*/
         };
    }

}
