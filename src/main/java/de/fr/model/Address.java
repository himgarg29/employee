package de.fr.model;

import java.util.Set;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;



@ApiModel(description = "All details about the Address model ")
@Entity
public class Address {
	
	@ApiModelProperty(notes = "The database generated address ID")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String streetName;
	private int houseNumber;
	
	@Column(name="EMP_ID", nullable=false)
	private Long employeeId;
	
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public int getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}
	
	

}
