package de.fr.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.fr.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long>  {


}
