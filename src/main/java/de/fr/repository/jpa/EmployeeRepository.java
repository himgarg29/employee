package de.fr.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import de.fr.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>  {

	List<Employee> findByFirstName(String firstName);
	
	@Query(value="SELECT emp FROM Employee emp where emp.firstName = ?1 or emp.lastName =?2")
	List<Employee> findByFirstNameOrLastName(String firstName, String lastName);
	
	//List<Employee> findByFirstNameOrLastName(String firstName, String lastName);
}
